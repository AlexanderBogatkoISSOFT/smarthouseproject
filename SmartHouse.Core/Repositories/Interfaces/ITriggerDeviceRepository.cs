﻿using SmartHouse.Models;

namespace SmartHouse.Core.Repositories.Interfaces
{
    public interface ITriggerDeviceRepository : IRepository<TriggerDevice>
    {

    }
}