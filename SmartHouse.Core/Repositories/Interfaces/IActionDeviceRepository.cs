﻿using SmartHouse.Models;

namespace SmartHouse.Core.Repositories.Interfaces
{
    public interface IActionDeviceRepository : IRepository<ActionDevice>
    {
    }
}
