﻿using SmartHouse.Models;
using System.Collections.Generic;

namespace SmartHouse.Core.Repositories.Interfaces
{
    public interface IRepository<T> where T: DbModel
    {
        IEnumerable<T> GetAll();
        T Get(int id);
        void Create(T item);
        void Update(T item);
        void Delete(int id);
    }
}