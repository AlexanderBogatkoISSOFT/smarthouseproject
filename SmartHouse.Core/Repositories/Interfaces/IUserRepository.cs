﻿using SmartHouse.Models;

namespace SmartHouse.Core.Repositories.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
    }
}