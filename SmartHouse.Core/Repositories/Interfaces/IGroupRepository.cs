﻿using SmartHouse.Models;

namespace SmartHouse.Core.Repositories.Interfaces
{
    public interface IGroupRepository : IRepository<Group>
    {
    }
}