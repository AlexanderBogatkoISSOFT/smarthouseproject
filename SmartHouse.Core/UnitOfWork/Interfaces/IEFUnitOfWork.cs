﻿using SmartHouse.Core.Repositories.Interfaces;

namespace SmartHouse.Core.UnitOfWork.Interfaces
{
    public interface IEFUnitOfWork
    {
        IUserRepository Users { get; }
        IGroupRepository Groups { get; }
        ITriggerDeviceRepository TriggerDevices { get; }
        IActionDeviceRepository ActionDevices { get; }
        void Save();
        void Dispose();
    }
}