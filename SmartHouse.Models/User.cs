﻿using System.Collections.Generic;

namespace SmartHouse.Models
{
    public class User : DbModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public IEnumerable<Group> Groups { get; set; }
    }
}