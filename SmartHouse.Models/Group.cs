﻿using System.Collections.Generic;

namespace SmartHouse.Models
{
    public class Group : DbModel
    {
        public string Name { get; set; }
        public TriggerDevice TriggerDevice { get; set; }
        public IEnumerable<ActionDevice> ActionDevices { get; set; }
    }
}