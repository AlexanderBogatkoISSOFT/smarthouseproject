﻿using SmartHouse.Core.Repositories.Interfaces;
using SmartHouse.Core.UnitOfWork.Interfaces;
using SmartHouse.Repositories;
using System;

namespace SmartHouse.UnitOfWork
{
    public class EFUnitOfWork : IEFUnitOfWork, IDisposable
    {
        private readonly SmartHouseDbContext _dbContext = new SmartHouseDbContext();
        private bool _disposed;
        private IUserRepository _userRepository;
        private IGroupRepository _groupRepository;
        private ITriggerDeviceRepository _triggerDeviceRepository;
        private IActionDeviceRepository _actionDeviceRepository;
        public IUserRepository Users => _userRepository ?? (_userRepository = new UserRepository(_dbContext));
        public IGroupRepository Groups => _groupRepository ?? (_groupRepository = new GroupRepository(_dbContext));
        public ITriggerDeviceRepository TriggerDevices => _triggerDeviceRepository ?? (_triggerDeviceRepository = new TriggerDeviceRepository(_dbContext));
        public IActionDeviceRepository ActionDevices => _actionDeviceRepository ?? (_actionDeviceRepository = new ActionDeviceRepository(_dbContext));
        public void Save()
        {
            _dbContext.SaveChanges();
        }
        public virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }
            if (disposing)
            {
                _dbContext.Dispose();
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}