﻿using SmartHouse.Models;
using SmartHouse.Core.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace SmartHouse.Repositories
{
    public class ActionDeviceRepository : BaseRepository, IActionDeviceRepository
    {
        public ActionDeviceRepository(SmartHouseDbContext dbContext) : base(dbContext)
        {
        }
        public void Create(ActionDevice actionDevice)
        {
            _dbContext.ActionDevices.Add(actionDevice);
        }

        public void Delete(int id)
        {
            var actionDevice = _dbContext.ActionDevices.Find(id);
            if (actionDevice != null)
                _dbContext.ActionDevices.Remove(actionDevice);
        }

        public ActionDevice Get(int id)
        {
            return _dbContext.ActionDevices.Find(id);
        }

        public IEnumerable<ActionDevice> GetAll()
        {
            return _dbContext.ActionDevices;
        }

        public void Update(ActionDevice actionDevice)
        {
            _dbContext.Entry(actionDevice).State = EntityState.Modified;
        }
    }
}