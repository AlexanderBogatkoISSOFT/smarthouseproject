﻿using Microsoft.EntityFrameworkCore;
using SmartHouse.Models;

namespace SmartHouse.Repositories
{
    public interface ISmartHouseDbContext
    {
        DbSet<User> Users { get; set; }
        DbSet<Group> Groups { get; set; }
        DbSet<TriggerDevice> TriggerDevices { get; set; }
        DbSet<ActionDevice> ActionDevices { get; set; }
    }
}