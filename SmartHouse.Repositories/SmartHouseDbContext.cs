﻿using Microsoft.EntityFrameworkCore;
using SmartHouse.Models;

namespace SmartHouse.Repositories
{
    public class SmartHouseDbContext : DbContext, ISmartHouseDbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<TriggerDevice> TriggerDevices { get; set; }
        public DbSet<ActionDevice> ActionDevices { get; set; }
        public SmartHouseDbContext() : base()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=SmartHouseDb;Trusted_Connection=True;");
        }
    }
}