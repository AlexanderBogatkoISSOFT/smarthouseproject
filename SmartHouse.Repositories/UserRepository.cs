﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using SmartHouse.Models;
using SmartHouse.Core.Repositories.Interfaces;

namespace SmartHouse.Repositories
{
    public class UserRepository : BaseRepository, IUserRepository
    {
        public UserRepository(SmartHouseDbContext dbContext) : base(dbContext)
        {
        }

        public void Create(User user)
        {
            _dbContext.Users.Add(user);
        }

        public void Delete(int id)
        {
            var user = _dbContext.Users.Find(id);
            if (user != null)
                _dbContext.Users.Remove(user);
        }

        public User Get(int id)
        {
            return _dbContext.Users.Find(id);
        }

        public IEnumerable<User> GetAll()
        {
            return _dbContext.Users;
        }

        public void Update(User user)
        {
            _dbContext.Entry(user).State = EntityState.Modified;
        }
    }
}