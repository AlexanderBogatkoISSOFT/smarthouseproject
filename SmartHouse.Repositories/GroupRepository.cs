﻿using SmartHouse.Models;
using SmartHouse.Core.Repositories.Interfaces;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace SmartHouse.Repositories
{
    public class GroupRepository : BaseRepository, IGroupRepository
    {
        public GroupRepository(SmartHouseDbContext dbContext) : base(dbContext)
        {
        }

        public void Create(Group group)
        {
            _dbContext.Groups.Add(group);
        }

        public void Delete(int id)
        {
            var group = _dbContext.Groups.Find(id);
            if (group != null)
                _dbContext.Groups.Remove(group);
        }

        public Group Get(int id)
        {
            return _dbContext.Groups.Find(id);
        }

        public IEnumerable<Group> GetAll()
        {
            return _dbContext.Groups;
        }

        public void Update(Group group)
        {
            _dbContext.Entry(group).State = EntityState.Modified;
        }
    }
}