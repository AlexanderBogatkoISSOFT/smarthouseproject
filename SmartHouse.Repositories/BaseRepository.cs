﻿namespace SmartHouse.Repositories
{
    public abstract class BaseRepository
    {
        protected readonly SmartHouseDbContext _dbContext;

        protected BaseRepository(SmartHouseDbContext dbContext)
        {
            _dbContext = dbContext;
        }
    }
}