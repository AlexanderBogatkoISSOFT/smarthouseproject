﻿using SmartHouse.Models;
using SmartHouse.Core.Repositories.Interfaces;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace SmartHouse.Repositories
{
    public class TriggerDeviceRepository : BaseRepository, ITriggerDeviceRepository
    {
        public TriggerDeviceRepository(SmartHouseDbContext dbContext) : base(dbContext)
        {
        }

        public void Create(TriggerDevice triggerDevice)
        {
            _dbContext.TriggerDevices.Add(triggerDevice);
        }

        public void Delete(int id)
        {
            var triggerDevice = _dbContext.TriggerDevices.Find(id);
            if (triggerDevice != null)
                _dbContext.TriggerDevices.Remove(triggerDevice);
        }

        public TriggerDevice Get(int id)
        {
            return _dbContext.TriggerDevices.Find(id);
        }

        public IEnumerable<TriggerDevice> GetAll()
        {
            return _dbContext.TriggerDevices;
        }

        public void Update(TriggerDevice triggerDevice)
        {
            _dbContext.Entry(triggerDevice).State = EntityState.Modified;
        }
    }
}