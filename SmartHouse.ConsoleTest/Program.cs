﻿using System;
using System.Collections.Generic;
using SmartHouse.Models;
using SmartHouse.UnitOfWork;

namespace SmartHouse.ConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var db = new EFUnitOfWork();
            for (int i = 0; i < 10; i++)
            {
                db.Users.Create(new User() { Email = $"Email{i}@gmail.com", Password = "123" });
            }
            db.Save();

            foreach (var item in db.Users.GetAll())
            {
                var groups = new List<Group>();
                for (int i = 0; i < 3; i++)
                {
                    groups.Add(new Group() { Name = $"group-{i}-{item.Id}" });
                }

                item.Groups = groups;

                foreach (var group in item.Groups)
                {
                    var trigger = new TriggerDevice { Name = $"trigger{group.Id}" };
                    var actionDevices = new List<ActionDevice>();
                    for(var i = 0; i < 3; i++)
                    {
                        actionDevices.Add(new ActionDevice() { Name = $"action-device-{group.Id}-{i}" });
                    }
                    group.TriggerDevice = trigger;
                    group.ActionDevices = actionDevices;
                }
            }

            db.Save();

            foreach (var user in db.Users.GetAll())
            {
                Console.WriteLine($"id: {user.Id}\t\tEmail:{user.Email}");

                foreach (var group in user.Groups)
                {
                    Console.WriteLine($"\t\tName: {group.Name}");
                    Console.WriteLine($"\t\t\tTrigger: {group.TriggerDevice.Name}");
                    foreach (var actionDevice in group.ActionDevices)
                    {
                        Console.WriteLine($"\t\t\tAction:{actionDevice.Name}");
                    }
                }
            }
        }
    }
}